package com.example.demo.controller;

import com.example.demo.Violation.Violation;

import com.example.demo.repository.ViolationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.stream.IntStream;


@RestController
@RequestMapping("/api/v1/fines")
public class UserController {

    @Autowired
    private ViolationRepo ViolationRepo;

    Violation fineContainer;

    @GetMapping("/{fineId}")
        public @ResponseBody ModelAndView getId(@PathVariable("fineId") int id, Model model){
        if (ViolationRepo.findById(id) != null){
            model.addAttribute("violation", ViolationRepo.findById(id));
            fineContainer = ViolationRepo.findById(id);
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("pageId.html");
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("erorTemp.html");
        return modelAndView;


    }
    @PatchMapping("/{fineId}/pay")
    public @ResponseBody String payFine(@PathVariable("fineId") int id) {
        if (ViolationRepo.findById(id) != null && !ViolationRepo.findById(id).getPayment()) {
            ViolationRepo.findById(id).setPayment(true);
            Violation a = ViolationRepo.findById(id);
            a.setPayment(true);
            ViolationRepo.save(a);
            return "Штраф оплачен";
        }
        return "Таокого id нет в базе или штраф уже оплачен.";
    }

    @PatchMapping("/{fineId}/court")
    public @ResponseBody String sendSubpoena(@PathVariable("fineId") int id) {
        if (ViolationRepo.findById(id) != null && !ViolationRepo.findById(id).getSubpoena()) {
            Violation a = ViolationRepo.findById(id);
            a.setSubpoena(true);
            ViolationRepo.save(a);
            return "Повестка отправлена";
        }
        return "Такого id нет в базе или повестка уже отправлена.";
    }

    @DeleteMapping()
    public String deleteContainer(@RequestBody Violation object) {
        if (fineContainer == null) {
            return "Сначала получите конкретный штраф через GET запрос по ID, а затем удалите его.";
        }
        ViolationRepo.delete(fineContainer);
        fineContainer = null;
        return "Штраф удален";
    }

    @PutMapping()
    public @ResponseBody String putContainer(@RequestBody Violation object) {
        if (fineContainer == null) {
            return "Сначала получите конкретный штраф через GET запрос по ID, а затем введите его новую конфигурацию.";
        }
        fineContainer.updateObject(object);
        ViolationRepo.save(fineContainer);
        fineContainer = null;
        return "Объект обновлен";
    }

    @PostMapping
    public @ResponseBody String post(@RequestBody Violation object) {
        ViolationRepo.save(object);
        return "Пользователь сохранен \n";
    }




    @GetMapping()
    public ModelAndView getAllUsers(Model model,
                                    @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                    @RequestParam(value = "sortField",required = false, defaultValue = "violatorName")  String sortField,
                                    @RequestParam (value = "sortDirection",required = false, defaultValue = "asc")  String sortDirection
                                    ) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending():
                Sort.by(sortField).descending();
        Page<Violation> violation = ViolationRepo.findAll(PageRequest.of(page, 3, sort));
        model.addAttribute("violation", violation);
        model.addAttribute("numbers", IntStream.range(0, violation.getTotalPages()).toArray());
        model.addAttribute("sortField", sortField);
        model.addAttribute("page", String.valueOf(page));
        model.addAttribute("sortDirection", sortDirection);
        model.addAttribute("reverseSortDir", sortDirection.equals("asc") ? "desc": "asc");

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("df.html");
        return modelAndView;

    }

}


