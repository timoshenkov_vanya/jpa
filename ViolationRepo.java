package com.example.demo.repository;

import com.example.demo.Violation.Violation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.domain.Pageable;

import java.util.List;
public interface ViolationRepo extends CrudRepository<Violation, Integer> {
    Violation findById(int id);
    Page<Violation> findAll(Pageable pageable);

}
